import json
import os
from utils.logger import log
from entry import Entry



def generate_wiki():
    # Load config.json
    with open("./code/config.json") as f:
        config = json.load(f)

    # Create Wiki Dirs
    log(type="info", message="Creating Wiki Directories...")
    models_path = config["MODELS_PATH"]
    wiki_path = config["WIKI_PATH"]
    os.makedirs(wiki_path, exist_ok=True)
    create_dirs(models_path, wiki_path)


def create_dirs(models_path, wiki_path):
    # Check if models_path exists
    if os.path.exists(models_path):
        for folder in os.listdir(models_path):
            folder_path = os.path.join(models_path, folder)
            if os.path.isdir(folder_path):
                create_md(folder_path, wiki_path)
                os.makedirs(os.path.join(wiki_path, folder), exist_ok=True)
                if folder_has_folders(folder_path):
                    create_dirs(folder_path, os.path.join(wiki_path, folder))
    # Remove any empty directories
    for folder in os.listdir(wiki_path):
        folder_path = os.path.join(wiki_path, folder)
        if os.path.isdir(folder_path):
            if os.listdir(folder_path) == []:
                os.rmdir(folder_path)
                

def create_md(models_path, wiki_path):
    # Load info from each .info file in the path as JSON
    header = "## [Back to Home](https://gitlab.com/Geoffery10/yume-wiki/-/wikis/home)\n\n"
    header += link_subfolders(models_path)
    text = header
    entries = 0
    entry_list = []
    for file in os.listdir(models_path):
        if file.endswith(".info"):
            try:
                with open(os.path.join(models_path, file)) as f:
                    civitai_info = json.load(f)
            except Exception as e:
                log(type="error", message=f"Failed to load {file}: {str(e)}")
                continue
            
            try:
                with open(os.path.join(models_path, file.replace(".civitai.info", ".json"))) as f:
                    json_info = json.load(f)
            except Exception as e:
                json_info = None
                # log(type="warning", message=f"Failed to load {file.replace('.civitai.info', '.json')}: {str(e)}")

            # Create an entry object
            temp_entry = Entry(file, civitai_info, json_info)
            temp_entry.setImage(models_path, wiki_path, file, temp_entry.name)
            entry_list.append(temp_entry)
            entries += 1
    if entries > 0:
        text += "\n\n\n ## Model Table of Contents\nShortcuts to the models in this folder:\n\n"
        for entry in entry_list:
            text += f"- {entry.getBookmark()}\n"
        text += "\n\n ## Models\n\n"
        for entry in entry_list:
            text += entry.getEntryText()
        text += header
    

    # Create a markdown file for the current folder
    md_name = os.path.basename(models_path)
    log(type="info", message=f"Writing Wiki File: {md_name}")
    if os.path.exists(os.path.join(wiki_path, f"{md_name}.md")):
        os.remove(os.path.join(wiki_path, f"{md_name}.md"))
    try:
        with open(os.path.join(wiki_path, f"{md_name}.md"), "w") as f:
            f.write(text)
    except Exception as e:
        log(type="error", message=f"Failed to write {md_name}.md: {str(e)}")
        return
    
def folder_has_folders(path):
    for file in os.listdir(path):
        if os.path.isdir(os.path.join(path, file)):
            return True
    return False

def get_subfolders(path):
    subfolders = []
    for file in os.listdir(path):
        if os.path.isdir(os.path.join(path, file)):
            subfolders.append(file)
    return subfolders

def link_subfolders(model_path):
    subfolders = get_subfolders(model_path)
    if subfolders == []:
        return ""
    links = "### Links to the sub-categories:\n\n"
    for folder in subfolders:
        path = model_path.split("\\")[-1]
        links += f"##### - [{folder.replace('_', ' ')}](./{path}/{folder})\n"
    # log(type="debug", message=f"Links: {links}")
    return links