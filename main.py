from utils.logger import log
from wiki_gen import generate_wiki
import os

def main():
    log(type="info", message="Updating Yume's Wiki...")
    generate_wiki()   
    
    generate_home()
    generate_sidebar()
        
    # Update the wiki git repo
    log(type="info", message="Pushing to GitLab...")
    os.system("cd wiki && git add . && git commit -m \"Update wiki\" && git push")


def generate_home():
    log(type="info", message="Updating Home.md...")
    loras = get_lora_root_folders()
    text = "## Lora Types\n\n"
    for lora in loras:
        sub_loras = get_lora_root_folders(f"./wiki/Lora/{lora['name']}")
        if sub_loras != []:
            text += f"<details><summary>{lora['md_link']}</summary>\n\n"
            for sub_lora in sub_loras:
                text += f"  - {sub_lora['html_link']}\n\n"
            text += "</details>\n\n"
        else:
            text += f"<details><summary>{lora['md_link']}</summary></details>\n\n"
    with open("./wiki/Home.md", "w") as f:
        header = "![Yume Pfp](https://cdn.discordapp.com/app-icons/1043957906921492562/249bc34c84c952a6e074f3d789414d33.png?size=128)\n"
        header += "# Yume's Wiki\n\n"
        header += "This is a wiki for Yume's LoRAs. It is automatically generated from the models folder. Please report bugs to geoffery10 on Discord.\n\n"
        header += "Look to the right for the sidebar shortcuts. :point_right:\n\n"
        header += "If you have requests for models please DM geoffery10 on Discord with the link to the Civitai Model (I reserve the right to decline requests).\n\n"
        header += "**WARNING: Some of these models are community added and may be capable of NSFW content.**\n\n"
        home = header + text
        f.write(home)


def generate_sidebar(): 
    log(type="info", message="Updating Sidebar.md...")
    home_link = "## [:house: Home](https://gitlab.com/Geoffery10/yume-wiki/-/wikis/home)\n"
    home_link += "### [:notebook: Best Practices](https://gitlab.com/Geoffery10/yume-wiki/-/wikis/best_practices)\n\n"
    home_link += "### :lower_left_paintbrush: Lora Types\n\n"
    home_link += "----\n\n"
    loras = get_lora_root_folders()
    for lora in loras:
        sub_loras = get_lora_root_folders(f"./wiki/Lora/{lora['name']}")
        if sub_loras != []:
            home_link += f"#### {lora['md_link']}\n\n"
            for sub_lora in sub_loras:
                home_link += f"- {sub_lora['html_link']}\n\n"
        else:
            home_link += f"#### {lora['md_link']}\n\n"
    with open("./wiki/_sidebar.md", "w") as f:
        f.write(home_link)
    
    
def get_lora_root_folders(path="./wiki/Lora"):
    loras = []
    log(type="info", message=f"Getting folders in {path}")
    for file in os.listdir(path):
        if file.endswith(".md"):
            loras.append({
                "name": file.split('.')[0],
                "md_link": f"[{file.split('.')[0]}]({path.replace('wiki/', '')}/{file.split('.')[0]})",
                "html_link": f"<a href=\"{path.replace('wiki/', '')}/{file.split('.')[0]}\">{file.split('.')[0]}</a>"
            })
    return loras
     
       
if __name__ == "__main__":
    main()
    log(type="info", message="Done!")