# Entry object used to fetch data from a civitai.info file and create an entry in the wiki.
import os
from utils.logger import log

class Entry:
    civitai_info = {}
    json = {}
    name = ""
    image = ""
    activator = ""
    trainedWords = ""
    promptExample = ""
    preferredWeight = 0
    
    def __init__(self, file, civitai_info, json_info=None):
        self.civitai_info = civitai_info
        self.json = json_info
        self.setName(civitai_info, file)
        self.image = ""
        self.setActivator()
        self.setTrainedWords(civitai_info)
        self.setPromptExample(civitai_info)
        self.setDescription(civitai_info)
        self.setWebsite(civitai_info)
        
        if self.json != None:
            self.setPreferredWeight(json_info)
        
    def setName(self, info, file):
        try:
            self.name = file.replace(".civitai.info", "")
            return
        except Exception as e:
            self.name = "ERROR"
        try:
            self.name = info["files"][0]["name"]
            return
        except Exception as e:
            self.name = "ERROR"
        try:
            self.name = info["name"]
            return
        except Exception as e:
            self.name = "ERROR"
        log(type="error", message=f"Failed to set name for {file}")
            
    def setImage(self, models_path, wiki_path, file, name):
        png = self.getPngOrPreview(models_path, file)
        png_path = os.path.join(models_path, png)
        wiki_full_path = os.path.join(wiki_path, models_path.split("\\")[-1])
        # log(type="debug", message=f"Copying {png} to {wiki_full_path}")
        if not os.path.exists(wiki_full_path):
            os.makedirs(f"{wiki_full_path}", exist_ok=True)
        # Copy png to wiki path
        os.system(f"cp \"{png_path}\" \"{wiki_full_path}\\{png}\"")
        base_path = os.path.join(models_path.split("\\")[-1], png)
        base_path = base_path.replace("\\", "/")
        self.image = f"<img src=\"./{base_path}\" alt=\"{name.split('.')[0]}\" width=\"160\"/>\n\n"
    
    
    def getPngOrPreview(self, models_path, file):
        png = file.replace(".civitai.info", ".png")
        if not os.path.exists(os.path.join(models_path, png)):
            png = file.replace(".civitai.info", ".preview.png")
        return png
    
        
    def setActivator(self):
        if self.preferredWeight == 0:
            self.activator = f'<lora:{self.name.split(".")[0]}:1>'
        else:
            self.activator = f'<lora:{self.name.split(".")[0]}:{self.preferredWeight}>'
        
    def setTrainedWords(self, info):
        try:
            self.trainedWords = ", ".join(info["trainedWords"])
        except Exception as e:
            log(type="error", message=f"Failed to set trained words: {str(e)}")
            self.trainedWords = "ERROR"
        if self.trainedWords == "":
            self.trainedWords = "N/A"
            
    def setPromptExample(self, info):
        try:
            self.promptExample = str(info["images"][0]["meta"]["prompt"])
            return
        except:
            self.promptExample = "N/A"
        try:
            self.promptExample = str(info["images"][1]["meta"]["prompt"])
            return
        except:
            self.promptExample = "N/A"
        # Search json for prompt 
        try:
            self.promptExample = info.get("prompt", "N/A")
            return
        except:
            self.promptExample = "N/A"
            
    def setDescription(self, info):
        try:
            self.description = info["model"]["description"]
            return
        except Exception as e:
            self.description = "N/A"
            
    def setWebsite(self, info):
        try:
            if info["modelId"] ==  "":
                self.website = "N/A"
            else:
                self.website = f"https://civitai.com/models/{info['modelId']}"
        except Exception as e:
            self.website = "N/A"
            
    def setPreferredWeight(self, json):
        try:
            self.preferredWeight = json["preferred weight"]
        except Exception as e:
            self.preferredWeight = 0
        self.setActivator()
        
    def getName(self):
        return str(self.name.encode("ascii", "ignore").decode().replace(".safetensors", "").replace(".ckpt", ""))
    
    def getBookmark(self):
        return f"[{self.getName()}](#{self.getName().lower().replace(' ', '-').replace('.', '')})"
    
    def isMyModel(self):
        # model_names = ['scottpilgrimvstheworld',
        #                'LostRuins', 'geoffery10_v1', 'MitsukiVRC', 'pamer_v1', 'SakuraVRC', 'SiriusVRC', 'SiriusVRC_v3', 'winterstargamer', 'winterstargamer_v2', 'rolo_v1']
        # if self.getName() in model_names:
        #     return True
        return False
        
    def getTextDescription(self, text=""):
        if self.description != "N/A":
            text += f"<details><summary><strong>Description from Website:</strong></summary>\n\n"
            for line in self.description.split("\n"):
                if line != "" and "ko-fi" not in line:
                    text += f"> {line}\n".replace("\n\n", "\n> \n")
            text += "</details>\n\n"
        return text
            
            
    def getEntryText(self):
        if self.isMyModel():
            text = f"## {self.getName()} by [Geoffery10](https://civitai.com/user/geoffery10/models)\n\n"
        else:
            text = f"## {self.getName()}\n\n"
        text += self.image
        text += f"**Activator:** `{self.activator}`\n\n"
        if self.trainedWords != "N/A":
            text += f"**Trained Words:** `{self.trainedWords}`\n\n"
        if self.promptExample != "N/A":
            text += f"<details><summary><strong>Prompt Example:</strong></summary>\n\n`{self.promptExample}`</details>\n\n"
        text += self.getTextDescription()
        if self.trainedWords != "N/A":
            text += f"**Copy to Use:** `{self.activator}, {self.trainedWords}`\n\n"
        else:
            text += f"**Copy to Use:** `{self.activator}`\n\n"
        if self.website != "N/A":
            text += f"**Website (MAY BE NSFW):** {self.website}\n\n"
        
        # Remove Bad Unicode Characters
        text = text.encode("ascii", "ignore").decode()
        return str(text)

